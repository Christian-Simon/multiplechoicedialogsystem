﻿// MultipleChoiceDialogSystem - Tuple.cs
// 
// Created by Christian Simon
// 
// Date 2015-09-17

namespace MultipleChoiceDialogSystem.Core
{
    /// <summary>
    ///     Represents a specific tuple build of a string and an int value.
    /// </summary>
    internal class Tuple
    {
        #region Properties

        /// <summary>
        ///     The message of the option.
        /// </summary>
        public string Message { get; private set; }

        /// <summary>
        ///     The following ID of the entry when this option is chosen.
        /// </summary>
        public int Follower { get; private set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Specific constructor.
        /// </summary>
        /// <param name="message">The message of the option.</param>
        /// <param name="follower">The ID of the following entry.</param>
        public Tuple(string message, int follower)
        {
            Message = message;

            Follower = follower;
        }

        #endregion
    }
}