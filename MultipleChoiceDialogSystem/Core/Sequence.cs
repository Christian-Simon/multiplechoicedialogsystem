﻿// MultipleChoiceDialogSystem - Sequence.cs
// 
// Created by Christian Simon
// 
// Date 2015-09-17

using System.Collections.Generic;
using System.Linq;

namespace MultipleChoiceDialogSystem.Core
{
    /// <summary>
    ///     Represents a sequence which is a chain of entries.
    /// </summary>
    public class Sequence
    {
        #region Variables

        /// <summary>
        ///     Represents the active entry by ID.
        /// </summary>
        private int activeEntryId;

        #endregion

        #region Properties

        /// <summary>
        ///     Contains all entries.
        /// </summary>
        private Dictionary<int, Entry> Entries { get; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Default constructor.
        /// </summary>
        public Sequence()
        {
            activeEntryId = -1;

            Entries = new Dictionary<int, Entry>(0);
        }

        #endregion

        #region Functions

        /// <summary>
        ///     Creates and adds an entry.
        /// </summary>
        /// <param name="entryId">The ID of the newly created entry.</param>
        /// <param name="speaker">The speaker of the newly created entry.</param>
        /// <param name="message">The message of the newly created entry.</param>
        /// <param name="followerId">The ID of the following entry.</param>
        public void AddEntry(int entryId, string speaker, string message, int followerId)
        {
            // Entry does not exist
            if (!Entries.ContainsKey(entryId))
            {
                // Creating entry
                Entries.Add(entryId, new Entry(speaker));
            }

            // Adding the option
            Entries[entryId].AddOption(message, followerId);
        }

        /// <summary>
        ///     removes an entry by id.
        /// </summary>
        /// <param name="entryId">The ID of the entry which should be removed.</param>
        public void RemoveEntry(int entryId)
        {
            // Removing the entry
            Entries.Remove(entryId);

            // TODO remove follower entries
        }

        /// <summary>
        ///     Sets the active entry to a new ID.
        /// </summary>
        /// <param name="entryId">The ID of the new active entry.</param>
        public void NextEntry(int entryId)
        {
            // Checking if ID exists otherwise it will be -1
            activeEntryId = Entries.ContainsKey(entryId) ? entryId : -1;
        }

        /// <summary>
        ///     Sets the active entry to the first possible entry.
        /// </summary>
        public void FirstEntry()
        {
            // Setting the active entry to the first possible entry
            activeEntryId = Entries.First().Key;
        }

        /// <summary>
        ///     Returns all options within an entry.
        /// </summary>
        /// <returns>Options of an entry.</returns>
        public Dictionary<string, int> GetOptions()
        {
            // Nothing is selected
            if (activeEntryId == -1)
            {
                return null;
            }

            return Entries[activeEntryId].GetOptions();
        }

        /// <summary>
        ///     Returns the speaker of the active entry.
        /// </summary>
        /// <returns>The speaker of the entry.</returns>
        public string GetSpeaker()
        {
            // Nothing is selected
            if (activeEntryId == -1)
            {
                return null;
            }

            return Entries[activeEntryId].GetSpeaker();
        }

        #endregion
    }
}