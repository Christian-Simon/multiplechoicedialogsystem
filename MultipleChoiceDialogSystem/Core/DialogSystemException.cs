﻿// MultipleChoiceDialogSystem - DialogSystemException.cs
// 
// Created by Christian Simon
// 
// Date 2016-01-19

using System;

namespace MultipleChoiceDialogSystem.Core
{
    /// <summary>
    ///     Represents the class used for dialog system related exception handling.
    /// </summary>
    public class DialogSystemException : Exception
    {
        #region Constructors

        /// <summary>
        ///     Exception constructor with simple string message.
        /// </summary>
        /// <param name="message">The message of the exception.</param>
        public DialogSystemException(string message) : base(message)
        {
        }

        /// <summary>
        ///     Exception constructor with simple string message and inner exception.
        /// </summary>
        /// <param name="message">The message of the exception.</param>
        /// <param name="inner">The inner exception.</param>
        public DialogSystemException(string message, Exception inner) : base(message, inner)
        {
        }

        #endregion
    }
}