﻿// MultipleChoiceDialogSystem - DialogSystem.cs
// 
// Created by Christian Simon
// 
// Date 2015-09-17

using System.Collections.Generic;
using System.Linq;
using MultipleChoiceDialogSystem.Events;
using MultipleChoiceDialogSystem.FileReader;

namespace MultipleChoiceDialogSystem.Core
{
    /// <summary>
    ///     Represents the dialog system which contains all data.
    /// </summary>
    public class DialogSystem
    {
        #region Variables

        /// <summary>
        ///     Represents the active sequence by ID.
        /// </summary>
        private int activeSequenceId;

        #endregion

        #region Properties

        /// <summary>
        ///     Contains all sequences.
        /// </summary>
        private Dictionary<int, Sequence> Sequences { get; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Default constructor.
        /// </summary>
        public DialogSystem()
        {
            activeSequenceId = -1;

            Sequences = new Dictionary<int, Sequence>(0);

            RegisterEvents();
        }

        #endregion

        #region Functions

        /// <summary>
        ///     Private function which registers all events at the event manager.
        /// </summary>
        private void RegisterEvents()
        {
            // Registering events
            DialogEventManager.Instance.AddListener<SequenceAddRequest>(OnSequenceAddRequest);
            DialogEventManager.Instance.AddListener<SequenceRemoveRequest>(OnSequenceRemoveRequest);
            DialogEventManager.Instance.AddListener<SequenceNextRequest>(OnSequenceNextRequest);
            DialogEventManager.Instance.AddListener<SequenceFirstRequest>(OnSequenceFirstRequest);
            DialogEventManager.Instance.AddListener<EntryAddRequest>(OnEntryAddRequest);
            DialogEventManager.Instance.AddListener<EntryRemoveRequest>(OnEntryRemoveRequest);
            DialogEventManager.Instance.AddListener<EntryNextRequest>(OnEntryNextRequest);
            DialogEventManager.Instance.AddListener<EntryFirstRequest>(OnEntryFirstRequest);
            DialogEventManager.Instance.AddListener<FileReadRequest>(OnFileReadRequest);
        }

        /// <summary>
        ///     Is called whenever a sequence should be added to the system.
        /// </summary>
        /// <param name="e">The actual event.</param>
        private void OnSequenceAddRequest(SequenceAddRequest e)
        {
            // Sequence does not exist
            if (!Sequences.ContainsKey(e.SequenceId))
            {
                // Creating the sequence
                Sequences.Add(e.SequenceId, new Sequence());
            }
        }

        /// <summary>
        ///     Is called whenever a sequence should be removed from the system.
        /// </summary>
        /// <param name="e">The actual event.</param>
        private void OnSequenceRemoveRequest(SequenceRemoveRequest e)
        {
            // Removing the sequence
            Sequences.Remove(e.SequenceId);
        }

        /// <summary>
        ///     Is called whenever the active sequence should be changed.
        /// </summary>
        /// <param name="e">The actual event.</param>
        private void OnSequenceNextRequest(SequenceNextRequest e)
        {
            // Checking if ID exists otherwise it will be -1
            activeSequenceId = Sequences.ContainsKey(e.SequenceId) ? e.SequenceId : -1;
        }

        /// <summary>
        ///     Is called whenever the active sequence should be set to the first possible sequence.
        /// </summary>
        /// <param name="e">The actual event.</param>
        private void OnSequenceFirstRequest(SequenceFirstRequest e)
        {
            // Setting the active sequence ID
            activeSequenceId = (Sequences.Count != 0) ? Sequences.First().Key : -1;
        }

        /// <summary>
        ///     Is called whenever a new entry should be added to the system.
        /// </summary>
        /// <param name="e">The actual event.</param>
        private void OnEntryAddRequest(EntryAddRequest e)
        {
            // Sequence does not exist
            if (!Sequences.ContainsKey(e.SequenceId))
            {
                Sequences.Add(e.SequenceId, new Sequence());
            }

            // Adding the entry
            Sequences[e.SequenceId].AddEntry(e.EntryId, e.Speaker, e.Message, e.FollowerId);
        }

        /// <summary>
        ///     Is called whenever an existing entry should be removed from the system.
        /// </summary>
        /// <param name="e">The actual event.</param>
        private void OnEntryRemoveRequest(EntryRemoveRequest e)
        {
            // Sequence ID exists
            if (Sequences.ContainsKey(e.SequneceId))
            {
                Sequences[e.SequneceId].RemoveEntry(e.EntryId);
            }
        }

        /// <summary>
        ///     Is called whenever the active entry should be changed.
        /// </summary>
        /// <param name="e">The actual event.</param>
        private void OnEntryNextRequest(EntryNextRequest e)
        {
            // Checking if a sequence is active
            if (activeSequenceId != -1)
            {
                // Setting the next active entry
                Sequences[activeSequenceId].NextEntry(e.EntryId);
            }
        }

        /// <summary>
        ///     Is called whenever the active entry should be set to the first possible.
        /// </summary>
        /// <param name="e">The actual event.</param>
        private void OnEntryFirstRequest(EntryFirstRequest e)
        {
            // Active sequence is set
            if (activeSequenceId != -1)
            {
                // Setting the active entry
                Sequences[activeSequenceId].FirstEntry();
            }
        }

        /// <summary>
        ///     Is called whenever a file should be loaded to the system.
        /// </summary>
        /// <param name="e">The actual event.</param>
        private void OnFileReadRequest(FileReadRequest e)
        {
            // Reading file
            Reader.ReadFile(e.FilePath);
        }

        /// <summary>
        ///     Returns all options of the active sequence and the active entry.
        /// </summary>
        /// <returns>Options of the active sequence and entry.</returns>
        public Dictionary<string, int> GetOptions()
        {
            // Nothing is selected
            if (activeSequenceId == -1)
            {
                return null;
            }

            return Sequences[activeSequenceId].GetOptions();
        }

        /// <summary>
        ///     Returns the speaker of the active sequence and the active entry.
        /// </summary>
        /// <returns>The speaker of the active sequence and entry.</returns>
        public string GetSpeaker()
        {
            // Nothing is selected
            if (activeSequenceId == -1)
            {
                return null;
            }

            return Sequences[activeSequenceId].GetSpeaker();
        }

        #endregion
    }
}