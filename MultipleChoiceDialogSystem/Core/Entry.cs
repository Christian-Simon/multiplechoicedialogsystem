﻿// MultipleChoiceDialogSystem - Entry.cs
// 
// Created by Christian Simon
// 
// Date 2015-09-17

using System.Collections.Generic;

namespace MultipleChoiceDialogSystem.Core
{
    /// <summary>
    ///     Represents an entry of the dialog system.
    /// </summary>
    internal class Entry
    {
        #region Properties

        /// <summary>
        ///     Represents the speaker of the entry.
        /// </summary>
        private string Speaker { get; }

        /// <summary>
        ///     List of options which basically are tuples of string and int.
        /// </summary>
        private List<Tuple> Options { get; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Default constructor.
        /// </summary>
        /// <param name="speaker">The speaker of the entry.</param>
        public Entry(string speaker)
        {
            Speaker = speaker;

            Options = new List<Tuple>(0);
        }

        #endregion

        #region Functions

        /// <summary>
        ///     Adds an option to the entry.
        /// </summary>
        /// <param name="message">The message which will be shown.</param>
        /// <param name="followerId">The ID of the following entry when this option is chosen.</param>
        public void AddOption(string message, int followerId)
        {
            Options.Add(new Tuple(message, followerId));
        }

        /// <summary>
        ///     Gathers all options of this entry and returns them using a Dictionary.
        /// </summary>
        /// <returns>All options of this entry.</returns>
        public Dictionary<string, int> GetOptions()
        {
            // No options exist
            if (Options.Count == 0)
            {
                return null;
            }

            var temp = new Dictionary<string, int>(1);

            // Gathering data
            foreach (var t in Options)
            {
                temp.Add(t.Message, t.Follower);
            }

            return temp;
        }

        /// <summary>
        ///     Returns the speaker of the entry.
        /// </summary>
        /// <returns>The speaker of the entry.</returns>
        public string GetSpeaker()
        {
            return Speaker;
        }

        #endregion
    }
}