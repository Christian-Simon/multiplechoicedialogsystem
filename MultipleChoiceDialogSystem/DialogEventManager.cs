﻿// MultipleChoiceDialogSystem - DialogEventManager.cs
// 
// Created by Christian Simon
// 
// Date 2015-09-09

using System;
using System.Collections.Generic;
using MultipleChoiceDialogSystem.Core;
using MultipleChoiceDialogSystem.Events;

namespace MultipleChoiceDialogSystem
{
    /// <summary>
    ///     Event system based on http://www.willrmiller.com/?p=87.
    /// </summary>
    public class DialogEventManager
    {
        #region Delegates

        /// <summary>
        ///     Delegate used in public space.
        /// </summary>
        /// <typeparam name="T">Type of ADialogEvent.</typeparam>
        /// <param name="e">The actual event.</param>
        public delegate void EventDelegate<T>(T e) where T : ADialogEvent;

        #endregion

        #region Static Variables and Constants

        /// <summary>
        ///     Represents the instance of the dialog system class.
        /// </summary>
        private static DialogSystem dialogSystem;

        /// <summary>
        ///     Represents the instance of the DialogManager class.
        /// </summary>
        private static DialogEventManager singleInstance;

        #endregion

        #region Variables

        /// <summary>
        ///     Represents the delegate lookup table.
        /// </summary>
        private readonly Dictionary<Delegate, EventDelegate> delegateLookup = new Dictionary<Delegate, EventDelegate>(0);

        /// <summary>
        ///     Represents the dictionary of delegates.
        /// </summary>
        private readonly Dictionary<Type, EventDelegate> delegates = new Dictionary<Type, EventDelegate>(0);

        #endregion

        #region Properties

        /// <summary>
        ///     Returns the instance of the dialog manager class if it exists otherwise it will be created and then returned.
        /// </summary>
        public static DialogEventManager Instance
        {
            get
            {
                if (singleInstance != null)
                {
                    // Instance is existing
                    return singleInstance;
                }

                // Creating instance of the event system
                singleInstance = new DialogEventManager();

                // Creating the dialog system
                dialogSystem = new DialogSystem();

                return singleInstance;
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        ///     Constructor.
        /// </summary>
        private DialogEventManager()
        {
        }

        #endregion

        #region Functions

        /// <summary>
        ///     Adds a listener to the event system.
        /// </summary>
        /// <typeparam name="T">Type of ADialogEvent.</typeparam>
        /// <param name="del">The function which will be used by the delegate.</param>
        public void AddListener<T>(EventDelegate<T> del) where T : ADialogEvent
        {
            // Checking for existing delegate
            if (delegateLookup.ContainsKey(del))
            {
                return;
            }

            // Creating a non-generic delegate which calls the generic one
            EventDelegate internalDelegate = e => del((T) e);

            // Adding the delegate to the lookup table
            delegateLookup[del] = internalDelegate;

            EventDelegate tempDelegate;

            if (delegates.TryGetValue(typeof(T), out tempDelegate))
            {
                // Combining existing and new delegates
                delegates[typeof(T)] = tempDelegate += internalDelegate;
            }
            else
            {
                // Adding new delegate
                delegates[typeof(T)] = internalDelegate;
            }
        }

        /// <summary>
        ///     Invokes the given event.
        /// </summary>
        /// <param name="e">The actual event which should be invoked.</param>
        public void Invoke(ADialogEvent e)
        {
            // Returning if event is invalid
            if (e == null)
            {
                return;
            }

            EventDelegate del;

            if (delegates.TryGetValue(e.GetType(), out del))
            {
                // Invoking the event
                del.Invoke(e);
            }
        }

        /// <summary>
        ///     Removes the listener from the event system.
        /// </summary>
        /// <typeparam name="T">Type of ADialogEvent.</typeparam>
        /// <param name="del">The function which will be removed from the delegate.</param>
        public void RemoveListener<T>(EventDelegate<T> del) where T : ADialogEvent
        {
            EventDelegate internalDelegate;

            // Checking for existing delegates
            if (delegateLookup.TryGetValue(del, out internalDelegate))
            {
                EventDelegate tempDelegate;

                if (delegates.TryGetValue(typeof(T), out tempDelegate))
                {
                    // ReSharper disable once DelegateSubtraction
                    tempDelegate -= internalDelegate;

                    if (tempDelegate == null)
                    {
                        // Removing delegate entry
                        delegates.Remove(typeof(T));
                    }
                    else
                    {
                        // Replacing delegate entry
                        delegates[typeof(T)] = tempDelegate;
                    }
                }

                // Removing delegate lookup entry
                delegateLookup.Remove(del);
            }
        }

        /// <summary>
        ///     Returns the gathered options from the active entry.
        /// </summary>
        /// <returns>Options of the active entry.</returns>
        public Dictionary<string, int> GetOptions()
        {
            return dialogSystem.GetOptions();
        }

        /// <summary>
        ///     Returns the active speaker of the active entry.
        /// </summary>
        /// <returns>Speaker of the active entry.</returns>
        public string GetSpeaker()
        {
            return dialogSystem.GetSpeaker();
        }

        #endregion

        #region Nested type: EventDelegate

        /// <summary>
        ///     Delegate used in private space.
        /// </summary>
        /// <param name="e">The actual event.</param>
        private delegate void EventDelegate(ADialogEvent e);

        #endregion
    }
}