﻿// MultipleChoiceDialogSystem - Reader.cs
// 
// Created by Christian Simon
// 
// Date 2015-09-17

using System;
using System.IO;
using System.Security;
using System.Xml;
using MultipleChoiceDialogSystem.Core;
using MultipleChoiceDialogSystem.Events;

namespace MultipleChoiceDialogSystem.FileReader
{
    /// <summary>
    ///     Represents the reader class for reading XML files.
    /// </summary>
    internal class Reader
    {
        #region Functions

        /// <summary>
        ///     Reads the given XML file and invokes events to fill the database.
        /// </summary>
        /// <param name="filePath">The file path for the XML file.</param>
        public static void ReadFile(string filePath)
        {
            try
            {
                using (var reader = XmlReader.Create(filePath))
                {
                    // Local variable
                    var sequenceId = -1;

                    while (reader.Read())
                    {
                        if (reader.Name == "sequence")
                        {
                            // Reading and parsing data
                            int.TryParse(reader.GetAttribute("id"), out sequenceId);

                            // Sequence not
                            if (sequenceId == -1)
                            {
                                continue;
                            }

                            // Invoking an event to add a sequence
                            DialogEventManager.Instance.Invoke(new SequenceAddRequest(sequenceId));
                        }

                        if (reader.Name != "entry")
                        {
                            continue;
                        }

                        // Sequence not set
                        if (sequenceId == -1)
                        {
                            continue;
                        }

                        // Local variables
                        int entryId = -1, followerId = -1;

                        // Reading (and parsing) data
                        int.TryParse(reader.GetAttribute("id"), out entryId);

                        var speaker = reader.GetAttribute("speaker");

                        var message = reader.GetAttribute("message");

                        int.TryParse(reader.GetAttribute("follower"), out followerId);

                        // Invoking an event to add an entry
                        DialogEventManager.Instance.Invoke(new EntryAddRequest(sequenceId, entryId, speaker,
                            message,
                            followerId));
                    }
                }
            }
            catch (XmlException ex)
            {
                throw new DialogSystemException("XML file is invalid at line " + ex.LineNumber + ".", ex);
            }
            catch (InvalidCastException ex)
            {
                throw new DialogSystemException("Something went terribly wrong while casting " + ex.Message + ".", ex);
            }
            catch (FileNotFoundException ex)
            {
                throw new DialogSystemException("File not found at path " + ex.FileName + ".", ex);
            }
            catch (ArgumentNullException ex)
            {
                throw new DialogSystemException("File path is null.", ex);
            }
            catch (SecurityException ex)
            {
                throw new DialogSystemException("Reader has no privileges to read file at " + ex.Url + ".", ex);
            }
        }

        #endregion
    }
}