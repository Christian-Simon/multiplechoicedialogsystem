﻿// MultipleChoiceDialogSystem - ASequenceRequest.cs
// 
// Created by Christian Simon
// 
// Date 2015-09-12

namespace MultipleChoiceDialogSystem.Events
{
    /// <summary>
    ///     This is the abstract base class for all sequence related events.
    /// </summary>
    public abstract class ASequenceRequest : ADialogEvent
    {
    }

    /// <summary>
    ///     Represents the event for adding a sequence with id.
    /// </summary>
    public class SequenceAddRequest : ASequenceRequest
    {
        #region Properties

        /// <summary>
        ///     The unique ID of the sequence.
        /// </summary>
        public int SequenceId { get; private set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Represent the event for adding a sequence to the dialog system.
        /// </summary>
        /// <param name="sequenceId">Unique ID of the sequence.</param>
        public SequenceAddRequest(int sequenceId)
        {
            SequenceId = sequenceId;
        }

        #endregion
    }

    /// <summary>
    ///     Represents the event for removing a sequence by id.
    /// </summary>
    public class SequenceRemoveRequest : ASequenceRequest
    {
        #region Properties

        /// <summary>
        ///     The unique ID of the sequence.
        /// </summary>
        public int SequenceId { get; private set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Represents the event for removing a sequence from the dialog system.
        /// </summary>
        /// <param name="sequenceId">Unique ID of the sequence.</param>
        public SequenceRemoveRequest(int sequenceId)
        {
            SequenceId = sequenceId;
        }

        #endregion
    }

    /// <summary>
    ///     Represents the event for choosing the next active sequence.
    /// </summary>
    public class SequenceNextRequest : ASequenceRequest
    {
        #region Properties

        /// <summary>
        ///     The unique ID of the sequence.
        /// </summary>
        public int SequenceId { get; private set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Represents the event for selecting the next active sequence.
        /// </summary>
        /// <param name="sequenceId">Unique ID of the sequence.</param>
        public SequenceNextRequest(int sequenceId)
        {
            SequenceId = sequenceId;
        }

        #endregion
    }

    /// <summary>
    ///     Represents the event for selecting the first possible sequence.
    /// </summary>
    public class SequenceFirstRequest : ASequenceRequest
    {
    }
}