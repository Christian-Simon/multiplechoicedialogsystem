﻿// MultipleChoiceDialogSystem - AEntryEvent.cs
// 
// Created by Christian Simon
// 
// Date 2015-09-17

namespace MultipleChoiceDialogSystem.Events
{
    /// <summary>
    ///     This is the abstract base class for all entry related events.
    /// </summary>
    public abstract class AEntryEvent : ADialogEvent
    {
    }

    /// <summary>
    ///     Represents the event for adding an entry with id and message.
    /// </summary>
    public class EntryAddRequest : AEntryEvent
    {
        #region Properties

        /// <summary>
        ///     The unique ID of the sequence.
        /// </summary>
        public int SequenceId { get; private set; }

        /// <summary>
        ///     The unique ID of the entry.
        /// </summary>
        public int EntryId { get; private set; }

        /// <summary>
        ///     The speaker of the option.
        /// </summary>
        public string Speaker { get; private set; }

        /// <summary>
        ///     The message of the speaker.
        /// </summary>
        public string Message { get; private set; }

        /// <summary>
        ///     The ID of the following entry.
        /// </summary>
        public int FollowerId { get; private set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Represents the event for adding a entry to the dialog system.
        /// </summary>
        /// <param name="sequenceId">Unique ID of the sequence.</param>
        /// <param name="entryId">Unique ID of the entry.</param>
        /// <param name="speaker">Speaker of the option.</param>
        /// <param name="message">Message of the speaker.</param>
        /// <param name="followerId">Unique ID of the following entry.</param>
        public EntryAddRequest(int sequenceId, int entryId, string speaker, string message, int followerId)
        {
            SequenceId = sequenceId;

            EntryId = entryId;

            Speaker = speaker;

            Message = message;

            FollowerId = followerId;
        }

        #endregion
    }

    /// <summary>
    ///     Represents the event for removing an entry by id.
    /// </summary>
    public class EntryRemoveRequest : AEntryEvent
    {
        #region Properties

        /// <summary>
        ///     The unique ID of the sequence.
        /// </summary>
        public int SequneceId { get; private set; }

        /// <summary>
        ///     The unique ID of the entry.
        /// </summary>
        public int EntryId { get; private set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Represents the event for removing an entry from the dialog system.
        /// </summary>
        /// <param name="sequenceId">Unique ID of the sequence.</param>
        /// <param name="entryId">Unique ID of the entry.</param>
        public EntryRemoveRequest(int sequenceId, int entryId)
        {
            SequneceId = sequenceId;

            EntryId = entryId;
        }

        #endregion
    }

    /// <summary>
    ///     Represents the event for choosing the next active entry.
    /// </summary>
    public class EntryNextRequest : AEntryEvent
    {
        #region Properties

        /// <summary>
        ///     The unique ID of the entry.
        /// </summary>
        public int EntryId { get; private set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Represents the event for selecting the next active entry.
        /// </summary>
        /// <param name="entryId">Unique ID of the next entry.</param>
        public EntryNextRequest(int entryId)
        {
            EntryId = entryId;
        }

        #endregion
    }

    /// <summary>
    ///     Represents the event for choosing the first possible entry.
    /// </summary>
    public class EntryFirstRequest : AEntryEvent
    {
    }
}