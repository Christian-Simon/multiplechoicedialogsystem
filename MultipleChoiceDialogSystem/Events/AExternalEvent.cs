﻿// MultipleChoiceDialogSystem - AExternalEvent.cs
// 
// Created by Christian Simon
// 
// Date 2015-09-17

namespace MultipleChoiceDialogSystem.Events
{
    /// <summary>
    ///     This is the abstract base class for all external events.
    /// </summary>
    public abstract class AExternalEvent : ADialogEvent
    {
    }

    /// <summary>
    ///     Represents the event for reading a file.
    /// </summary>
    public class FileReadRequest : AExternalEvent
    {
        #region Properties

        /// <summary>
        ///     The path of the file.
        /// </summary>
        public string FilePath { get; private set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Represents the event for reading a XML file.
        /// </summary>
        /// <param name="filePath">Path of the file.</param>
        public FileReadRequest(string filePath)
        {
            FilePath = filePath;
        }

        #endregion
    }
}