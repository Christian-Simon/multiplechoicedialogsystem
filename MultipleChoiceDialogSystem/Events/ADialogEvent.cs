﻿// MultipleChoiceDialogSystem - ADialogEvent.cs
// 
// Created by Christian Simon
// 
// Date 2015-09-10

namespace MultipleChoiceDialogSystem.Events
{
    /// <summary>
    ///     This is the abstract base class for all derived events.
    /// </summary>
    public abstract class ADialogEvent
    {
    }
}