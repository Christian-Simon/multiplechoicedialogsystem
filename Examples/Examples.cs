﻿// Examples - Examples.cs
// 
// Created by Christian Simon
// 
// Date 2015-09-21

using System;
using System.Linq;
using MultipleChoiceDialogSystem;
using MultipleChoiceDialogSystem.Events;

namespace Examples
{
    /// <summary>
    ///     Represents the complete logic for use the dialog system as a text adventure.
    /// </summary>
    public class Examples
    {
        #region Functions

        /// <summary>
        ///     Main function which allows dragging XML file onto the executable to read its content.
        /// </summary>
        /// <param name="args">Empty or XML file dropped onto the executable.</param>
        public static void Main(string[] args)
        {
            // Loading file
            try
            {
                if (args[0] != null)
                {
                    DialogEventManager.Instance.Invoke(new FileReadRequest(args[0]));
                }
            }
            catch (IndexOutOfRangeException ex)
            {
                Console.WriteLine("No file dragged onto executable. Loading default file.");

                DialogEventManager.Instance.Invoke(new FileReadRequest("TextAdventure.xml"));
            }

            // Setting the active sequence and entry
            DialogEventManager.Instance.Invoke(new SequenceFirstRequest());
            DialogEventManager.Instance.Invoke(new EntryFirstRequest());

            WelcomeScreen();

            CoreLoop();

            // Exiting application
            Console.Clear();

            Console.WriteLine("Finished. Any key to quit.");

            Console.ReadLine();
        }

        /// <summary>
        ///     Represents the welcome screen of the text adventure.
        /// </summary>
        private static void WelcomeScreen()
        {
            Console.WriteLine();
            Console.WriteLine("--------------------");
            Console.WriteLine("Press ENTER to start");
            Console.WriteLine("--------------------");

            while (true)
            {
                var input = Console.ReadKey();

                // Starting game or dialog
                if (input.Key == ConsoleKey.Enter)
                {
                    return;
                }
            }
        }

        /// <summary>
        ///     Represents the core loop of the text adventure.
        /// </summary>
        private static void CoreLoop()
        {
            while (true)
            {
                var singleOption = false;

                if (DialogEventManager.Instance.GetOptions() != null)
                {
                    // Clearing the screen
                    Console.Clear();

                    // Determining if there is only a single option
                    if (DialogEventManager.Instance.GetOptions().Count == 1)
                    {
                        singleOption = true;
                    }

                    // Displaying speaker of the option(s)
                    if (DialogEventManager.Instance.GetSpeaker() != null)
                    {
                        Console.WriteLine(DialogEventManager.Instance.GetSpeaker() + ":");
                    }

                    // Displaying all possible options
                    foreach (var kvp in DialogEventManager.Instance.GetOptions())
                    {
                        // Single option
                        if (singleOption)
                        {
                            Console.WriteLine(kvp.Key);
                        }
                        // Multiple options
                        else
                        {
                            Console.WriteLine(kvp.Key + " [" + kvp.Value + "]");
                        }
                    }

                    // Input handler for single option
                    if (singleOption)
                    {
                        Console.Write("\nENTER to continue [ESC to quit]");

                        // Waiting for correct input key being pressed
                        while (true)
                        {
                            var input = Console.ReadKey();

                            // Selecting next entry
                            if (input.Key == ConsoleKey.Enter)
                            {
                                // Invoking new event to set next entry
                                var entryId = DialogEventManager.Instance.GetOptions().First().Value;

                                if (entryId == -1)
                                {
                                    // Exiting
                                    return;
                                }

                                DialogEventManager.Instance.Invoke(new EntryNextRequest(entryId));

                                break;
                            }
                            // Closing application
                            if (input.Key == ConsoleKey.Escape)
                            {
                                return;
                            }
                        }
                    }
                    // Input helper for multiple options
                    else
                    {
                        Console.Write("\nYour choice: ");

                        int input;

                        // Mapping input to an integer value
                        if (int.TryParse(Console.ReadLine(), out input))
                        {
                            if (input == -1)
                            {
                                // Exiting
                                return;
                            }

                            // Checking if input is possible
                            if (DialogEventManager.Instance.GetOptions().ContainsValue(input))
                            {
                                // Invoking new event to set the next entry
                                DialogEventManager.Instance.Invoke(new EntryNextRequest(input));
                            }

                            // Closing application
                            if (input == -1)
                            {
                                return;
                            }
                        }
                    }
                }
            }
        }

        #endregion
    }
}