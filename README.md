# README #

The Multiple Choice Dialog System (hereafter MCDS) helps you having dialogs with several options in your games. 

## Overview ##

MCDS is a small library which is easy to use and can be included in every C# application as well as Unity3D projects. Internally it makes use of an event system which is necessary for communication. In fact this event manager works as an interface to the application you are building.

## Core ##

MCDS is able to store multiple sequences whereat a sequence is basically a single dialog between multiple characters. That means that a sequence persists of several entries each having one or more options the player can choose from.

## Usage ##

First you need to create a XML file containing your dialogs. This basically looks like the following:

```xml
<dialogsystem>
  <sequence id="">
    <entry id="" speaker="optional" message="" follower="" />	
  </sequence> 
</dialogsystem>
```

In order to get things working correctly the 'sequence' node needs an unique ID. This represents also the value needed to access the sequence in game. The 'entry' node requires some more data besides an unique ID. The 'speaker' attribute is optional. When set it represents the speaker of the entry. The 'message' attribute contains the message which is said and the 'follower' attribute needs a valid value to an entry in order to create an entry chain. Examples can be found within the download archive.

Second step is to include the library to your project which is either a C# application or an Unity3D project.

Thirdly when having added the necessary references to your application you can use the following code to set and get data from the dialog system.

### Loading data: ###
```
#!c#
DialogEventManager.Instance.Invoke(new FileReadRequest("path_to_your_xml_file.xml"));
```

### Setting active sequence and entry: ###
```
#!c#
DialogEventManager.Instance.Invoke(new SequenceFirstRequest());
DialogEventManager.Instance.Invoke(new EntryFirstRequest());
```
	
### Changing active sequence and entry if necessary: ###
```
#!c#
DialogEventManager.Instance.Invoke(new SequenceNextRequest(id_of_the_sequence));
DialogEventManager.Instance.Invoke(new EntryNextRequest(id_of_the_entry));
```
	
### Getting the current speaker and messages of the active entry: ###
```
#!c#
DialogEventManager.Instance.GetSpeaker();
DialogEventManager.Instance.GetOptions();
```
	
For further usage look at the exmaples for C# applications or the Unity package for Unity3D Engine.

## Download ##

The download contains the source code and the compiled library file as well as a test application and a test package for the Unity3D Engine.

## Examples ##

The used example for the text adventure can be found over here: http://textadventures.co.uk/games/view/yxc4hu2qd0uehit8aqpmeq/the-heist

## The MIT License (MIT) ##

Copyright (c) 2015 Christian Simon

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.